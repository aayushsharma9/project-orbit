using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUIManager : MonoBehaviour
{
    public static GUIManager instance;
    [SerializeField] private GameObject GameOverPanel;
    void Start()
    {
        if (GUIManager.instance == null)
            GUIManager.instance = this;
        else
            Destroy(gameObject);

        GameOverPanel.SetActive(false);
    }

    public void GameOverPanelSetActive(bool active)
    {
        GameOverPanel.SetActive(active);
    }
}