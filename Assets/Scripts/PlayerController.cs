using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private float revSpeed;
    [SerializeField] private float rotSpeed;
    private bool inRangeOfAnchor, anchorInLOS;
    private GameObject nearestAnchor;
    private Rigidbody2D playerRigidbody;

    private float lastRevDirIn = 1;
    private float originalGravScale;

    void Start()
    {
        playerRigidbody = gameObject.GetComponent<Rigidbody2D>();
        originalGravScale = playerRigidbody.gravityScale;
    }

    void Update()
    {
        if (!inRangeOfAnchor) return;
        if (!anchorInLOS) return;
        nearestAnchor.GetComponent<AnchorBehaviour>().SetEnabled(true);

        float revDirIn = Input.GetAxis("Horizontal") == 0 ? lastRevDirIn : Input.GetAxis("Horizontal");
        if (revDirIn != 0) lastRevDirIn = revDirIn;
        int revDir = revDirIn > 0 ? 1 : -1;
        Vector3 dir = nearestAnchor.transform.position - transform.position;
        Vector2 cforce = Vector2.Perpendicular(new Vector2(dir.x, dir.y).normalized * revSpeed * revDir);

        if (Input.GetButton("Jump"))
        {
            playerRigidbody.velocity = cforce;
            // playerRigidbody.AddForce(cforce);
            playerRigidbody.gravityScale = 0;

            //Adjust orbit distance
            Vector2 playerToAnchorDir = transform.position - nearestAnchor.transform.position;
            float playerToAnchorDist = playerToAnchorDir.magnitude;
            if (playerToAnchorDist > nearestAnchor.GetComponent<AnchorBehaviour>().getOrbitDistUpper())
            {
                playerRigidbody.velocity += -playerToAnchorDir;
            }

            if (playerToAnchorDist < nearestAnchor.GetComponent<AnchorBehaviour>().getOrbitDistLower())
            {
                playerRigidbody.velocity += playerToAnchorDir;
            }
        }

        if (Input.GetButtonUp("Jump"))
        {
            playerRigidbody.gravityScale = originalGravScale;
            playerRigidbody.AddForce(cforce, ForceMode2D.Impulse);
        }

        float angle = Mathf.Atan2(playerRigidbody.velocity.y, playerRigidbody.velocity.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, 0, angle), rotSpeed * Time.deltaTime);
    }

    private void FixedUpdate()
    {
        if (!inRangeOfAnchor) return;
        Vector2 dir = nearestAnchor.transform.position - transform.position;
        RaycastHit2D[] hits = Physics2D.RaycastAll(transform.position, dir);
        for (int i = 0; i < hits.Length; i++)
        {
            // if (hits[i].collider.tag == "Obstacle") return;
            Debug.Log(i + "th hit: " + hits[i].collider.tag);
        }
        if (hits[0].collider != null)
        {
            Debug.Log("Just hit: " + hits[0].collider.tag);
            Debug.DrawRay(transform.position, dir, Color.white, 5);
            if (hits[0].collider.tag == "Anchor")
            {
                anchorInLOS = true;
            }
            else
            {
                anchorInLOS = false;
                nearestAnchor.GetComponent<AnchorBehaviour>().SetEnabled(false);
            }
        }
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Anchor")
        {
            //TODO: Compare distance to anchors when in range of multiple anchors, set nearestAnchor accordingly
            inRangeOfAnchor = true;
            nearestAnchor = other.gameObject;
            // nearestAnchor.GetComponent<AnchorBehaviour>().SetEnabled(true);
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Anchor")
        {
            inRangeOfAnchor = false;
            other.gameObject.GetComponent<AnchorBehaviour>().SetEnabled(false);
        }
    }
}