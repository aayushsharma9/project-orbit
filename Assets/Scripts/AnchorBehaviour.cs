using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnchorBehaviour : MonoBehaviour
{
    [SerializeField] private float range;
    [SerializeField] private float orbitDist;
    [SerializeField] private float orbitDistError;
    [SerializeField] GameObject enabledSprite;

    void Start()
    {
        enabledSprite.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public float getOrbitDistLower()
    {
        return orbitDist - orbitDistError;
    }

    public float getOrbitDistUpper()
    {
        return orbitDist + orbitDistError;
    }

    public void SetEnabled(bool enable)
    {
        enabledSprite.SetActive(enable);
    }
}