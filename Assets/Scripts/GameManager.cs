using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    [SerializeField] private int maxPoints;
    private int pointsCollected;

    void Start()
    {
        if (GameManager.instance == null)
            GameManager.instance = this;
        else
            Destroy(gameObject);
        pointsCollected = 0;
    }

    public void incrementPoints()
    {
        pointsCollected++;
    }

    public void incrementPoints(int n)
    {
        pointsCollected += n;
    }

    public void GameOverRoutine()
    {
        GUIManager.instance.GameOverPanelSetActive(true);
    }
}