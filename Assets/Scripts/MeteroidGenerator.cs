using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteroidGenerator : MonoBehaviour
{
    public GameObject objectPrefab;
    // public Vector3 position;

    void Start()
    {
        StartCoroutine( SpawnObjects() );
    }


    IEnumerator SpawnObjects()
    {
        while( true )
        {
            Instantiate( objectPrefab, transform.position, Quaternion.identity );
            Debug.Log(transform.position);
            yield return new WaitForSeconds( 3.0f );
        }
    }
}

