using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimedAnchorBehaviour : MonoBehaviour
{
    [SerializeField] private float maxLifeInSeconds;
    private float timer;

    void Start()
    {
        timer = 0;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player") StartCoroutine(DestroySelf());
    }
    IEnumerator DestroySelf()
    {
        yield return new WaitForSeconds(maxLifeInSeconds);
        GameObject.Destroy(gameObject);
    }

}