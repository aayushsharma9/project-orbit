using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehaviour : MonoBehaviour
{
    void Start()
    {

    }

    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        switch (other.tag)
        {
            case "Goal":
                //TODO: Handle UI Logic
                Debug.Log("Level Clear");
                break;
            case "Hazard":
                //TODO: Handle UI Logic
                GameManager.instance.GameOverRoutine();
                Debug.Log("Player ded");
                break;
            case "Collectible":
                GameManager.instance.incrementPoints();
                break;
            default:
                break;
        }
    }
}