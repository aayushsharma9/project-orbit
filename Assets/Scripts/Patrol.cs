using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patrol : MonoBehaviour
{
    public Vector3[] checkPoints;
    public float speed;
    private int destination;
    int dir = -1;

    void Start()
    {
        destination = 0;
        gameObject.transform.position = checkPoints[destination];
        destination++;
    }

    void Update()
    {
        Vector2 selfPos = new Vector2(gameObject.transform.position.x, gameObject.transform.position.y);
        Vector2 des = new Vector2(checkPoints[destination].x, checkPoints[destination].y);

        if (Mathf.Abs(Vector2.Distance(selfPos, des)) < 1)
        {
            transform.Rotate(0, 0, 90 * dir);
            dir *= -1;
            destination = (destination + 1) % checkPoints.Length;
        }

        Vector2 moveDir = des - selfPos;
        moveDir.Normalize();
        gameObject.transform.Translate(moveDir * speed * Time.deltaTime);
    }
}