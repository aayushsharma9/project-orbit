using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lazer : MonoBehaviour
{
    [SerializeField] float waitSeconds;

    void Start() 
    {
        Debug.Log(transform.GetChild(0).gameObject.active);
        {
            StartCoroutine( SpawnLazer() );
        }
    }


    IEnumerator SpawnLazer()
    {
        while( true )
        {
            transform.GetChild(0).gameObject.active = !transform.GetChild(0).gameObject.active;
            Debug.Log(transform.GetChild(0).gameObject.active);
            yield return new WaitForSeconds( waitSeconds );
        }
    }
}