using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VanishingAnchorBehaviour : MonoBehaviour
{
    [SerializeField] private float duration;
    private float t;
    private bool visible;

    void Start()
    {
        t = 0;
        visible = true;
    }

    void Update()
    {
        t += Time.deltaTime;

        if (t >= duration)
        {
            visible = !visible;
            ToggleVanish(visible);
            t = 0;
        }
    }

    void ToggleVanish(bool enabled)
    {
        Collider2D[] colliders = gameObject.GetComponents<Collider2D>();
        SpriteRenderer[] spriteRenderers = gameObject.GetComponentsInChildren<SpriteRenderer>();
        foreach (Collider2D collider in colliders)
        {
            collider.enabled = enabled;
        }

        foreach (SpriteRenderer spriteRenderer in spriteRenderers)
        {
            spriteRenderer.enabled = enabled;
        }
    }
}