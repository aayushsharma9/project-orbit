using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrollingRotation : MonoBehaviour
{
    int dir = 1;
    [SerializeField] float positiveBound = 30.0f;
    [SerializeField] float negativeBound = -30.0f;
    [SerializeField] float speed = 20.0f;

    void Update()
    {
        transform.Rotate(new Vector3(0, 0, dir) * Time.deltaTime * speed);
        float angle = (transform.rotation.eulerAngles.z > 180) ? (transform.rotation.eulerAngles.z - 360) : transform.rotation.eulerAngles.z;
        Debug.Log(transform.rotation.eulerAngles.z);
        if(angle >= positiveBound) dir = -1;
        else if (angle <= negativeBound) dir = 1;
    }
}